'use strict';

const fs = require('fs');
const path = require('path');

const ttf2woff = require('ttf2woff');
const ttf2woff2 = require('ttf2woff2');

const printTime = date => {
    return `[${String(date.getHours()).padStart(2, '0')}:` +
        `${String(date.getMinutes()).padStart(2, '0')}:` +
        `${String(date.getSeconds()).padStart(2, '0')}]`;
};

const startTime = new Date();
console.log(`${printTime(startTime)} Starting...`);

const inputDir = 'input';
const outputDir = 'output';

if (!fs.existsSync(outputDir)) fs.mkdirSync(outputDir);

let processedFonts = 0;
let convertedFonts = 0;

const finishTask = () => {
    convertedFonts += 1;
    if (convertedFonts < processedFonts) return;

    const endTime = new Date();
    console.log(`${printTime(endTime)} Finished after ${endTime.getTime() - startTime.getTime()} ms`);
};

fs.readdirSync(inputDir).forEach(file => {
    processedFonts += 2;
    const filePath = path.join(inputDir, file);

    switch (path.extname(file)){
        case '.ttf':
            const fileContent = fs.readFileSync(filePath);

            const woff = ttf2woff(fileContent);
            const woffContent = Buffer.from(woff.buffer);
            fs.writeFile(path.join(outputDir, path.basename(file, '.ttf') + '.woff'), woffContent, () => {
                finishTask();
            });

            const woff2Content = ttf2woff2(fileContent);
            fs.writeFile(path.join(outputDir, path.basename(file, '.ttf') + '.woff2'), woff2Content, () => {
                finishTask();
            });

            break;
        default:
            processedFonts -= 2;
    }
});